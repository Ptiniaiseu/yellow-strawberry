import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DemoMaterialModule } from '../demo-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { StarterComponent } from './starter.component';
import { StarterRoutes } from './starter.routing';
import { YagaModule, MarkerProvider } from '@yaga/leaflet-ng2';
import { HttpModule } from '@angular/http';
import { ControlPanelComponent } from './control-panel/control-panel.component';
import { ParkService } from '../shared/park.service';
import { ParkDetailsComponent } from './park-details/park-details.component';

@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    FlexLayoutModule,
    RouterModule.forChild(StarterRoutes),
    YagaModule,
    HttpModule
  ],
  declarations: [StarterComponent, ControlPanelComponent, ParkDetailsComponent],
  providers: [ParkService],
  entryComponents: [ControlPanelComponent, ParkDetailsComponent]
})

export class StarterModule { }
