import { LatLng } from "leaflet";

export class Filter {
    id: string
    label: string
    color: string
    parks: Array<any>

    constructor(id: string, label: string, parks: Array<any>) {
        this.id = id
        this.label = label;
        this.parks = parks.map(park => {
            let lng = 0;
            let lat = 0;
            park.geometry.coordinates[0].forEach(coordinate => {
                lng += coordinate[0];
                lat += coordinate[1]
            });

            lat = lat / park.geometry.coordinates[0].length;
            lng = lng / park.geometry.coordinates[0].length;

            park.lnglat = [lng, lat]
            return park
        })
        this.color = this.getRandomColor()
    }

    private getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
}