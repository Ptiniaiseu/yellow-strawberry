import { Injectable } from "@angular/core";
import { LatLng } from "leaflet";
import { HttpClient } from '@angular/common/http';
import { Filter } from "./filter.model";
import { Park } from "./park.model";
import { equalSegments } from "@angular/router/src/url_tree";
import { PointOfInterest } from "./point-of-interest.model";

@Injectable()
export class ParkService {

    parkNumberLimit = 10
    interests: Array<PointOfInterest> = new Array()
    bestParks: Array<Park> = new Array();
    buddyLocations: Array<LatLng> = new Array()
    selectedFilters: Array<Filter> = new Array();
    filters: Array<Filter> = new Array()
    leisures: Array<any> = new Array();

    leisureIcons = {
        "bench": "assets/images/pins/bench-pin.svg",
        "bicycle_parking": "assets/images/pins/bike-pin.svg",
        "drinking_water": "assets/images/pins/water-pin.svg",
        "picnic_table": "assets/images/pins/picnic-pin.svg",
        "playground": "assets/images/pins/playground-pin.svg",
        "track": "assets/images/pins/track-pin.svg",
        "pitch": "assets/images/pins/skate-pin.svg",
        "swimming_pool": "assets/images/pins/pool-pin.svg",
        "water_park": "assets/images/pins/water_park-pin.svg"
    }

    constructor(private http: HttpClient) {
        this.http.get("assets/realjson/Bench.geojson").subscribe((res: any) => this.filters.push(new Filter("bench", "Bench", res.features)));
        this.http.get("assets/realjson/BicycleParking.geojson").subscribe((res: any) => this.filters.push(new Filter("bicycle_parking", "Bicycle Parking", res.features)));
        this.http.get("assets/realjson/drinkingwater_rel.geojson").subscribe((res: any) => this.filters.push(new Filter("drinking_water", "Drinking Water", res.features)));
        this.http.get("assets/realjson/picnictable_rel.geojson").subscribe((res: any) => this.filters.push(new Filter("picnic_table", "Picnic Table", res.features)));
        this.http.get("assets/realjson/playground.geojson").subscribe((res: any) => this.filters.push(new Filter("playground", "Playground", res.features)));
        this.http.get("assets/realjson/RunningTrack.geojson").subscribe((res: any) => this.filters.push(new Filter("track", "Running Track", res.features)));
        this.http.get("assets/realjson/Skateboard.geojson").subscribe((res: any) => this.filters.push(new Filter("pitch", "Skateboard", res.features)));
        this.http.get("assets/realjson/swimmingpool_rel.geojson").subscribe((res: any) => this.filters.push(new Filter("swimming_pool", "Swimming Pool", res.features)));
        this.http.get("assets/realjson/Waterpark.geojson").subscribe((res: any) => this.filters.push(new Filter("water_park", "Waterpark", res.features)));

        this.http.get("assets/leisures.json").subscribe((res: any) => this.leisures = res.elements);
    }

    findPark(): any {
        let activeFilters = null;
        if (this.selectedFilters.length == 0)
            activeFilters = this.filters;
        else
            activeFilters = this.selectedFilters
        let lat = 0;
        let lng = 0;
        this.buddyLocations.forEach(el => {
            lat += el.lat;
            lng += el.lng;
        })
        const buddiesAvgPos = new LatLng(lat / this.buddyLocations.length, lng / this.buddyLocations.length);

        const parks: Array<Park> = new Array();
        activeFilters.forEach(filter => {
            const sortedParks = filter.parks.sort((park1, park2) => this.dist(park1.lnglat[1], park1.lnglat[0], buddiesAvgPos.lat, buddiesAvgPos.lng) - this.dist(park2.lnglat[1], park2.lnglat[0], buddiesAvgPos.lat, buddiesAvgPos.lng))
            for (let i = 0; i < sortedParks.length; i++) {
                const parkMeta = sortedParks[i];


                const exists = parks.findIndex(p => this.test(p, parkMeta));
                if (exists == -1) {
                    const park = new Park(parkMeta.properties.name, new LatLng(parkMeta.lnglat[1], parkMeta.lnglat[0]), this.parkNumberLimit - i, parkMeta);
                    park.filtersRequested.push(filter.label)
                    parks.push(park);
                }
                else {
                    parks[exists].addScore(this.parkNumberLimit - i);
                    parks[exists].filtersRequested.push(filter.label)
                }
            }
        });

        this.bestParks = parks.filter(park => park.filtersRequested.length == activeFilters.length);
        this.bestParks = this.bestParks.slice(0, this.parkNumberLimit);

    }

    findPointOfInterests(park: Park) {
        const radius = this.findRadius(park.metadata.geometry.coordinates, park.latlng);
        const POIs: Array<PointOfInterest> = new Array()
        this.leisures.forEach(leisure => {
            const distance = this.dist(leisure.lat, leisure.lon, park.latlng.lat, park.latlng.lng)
            if (distance < radius) {
                let id = null;
                if (leisure.tags && leisure.tags.leisure)
                    id = leisure.tags.leisure
                else if (leisure.tags && leisure.tags.amenity)
                    id = leisure.tags.amenity
                if (id != null) {
                    const filter = this.filters.find(el => el.id == id)
                    POIs.push(new PointOfInterest(this.leisureIcons[id], filter.label, new LatLng(leisure.lat, leisure.lon), leisure))
                }
            }
        });

        this.interests = POIs;
    }

    findRadius(geometries: Array<Array<Array<number>>>, center: LatLng): number {
        let coordinates: Array<Array<number>> = new Array();
        geometries.forEach(geometry => {
            coordinates = coordinates.concat(geometry);
        });

        const sorted = coordinates.sort((park1, park2) => this.dist(park1[1], park1[0], center.lat, center.lng) - this.dist(park2[1], park2[0], center.lat, center.lng))

        const farthest = sorted[sorted.length - 1];
        const radius = this.dist(farthest[1], farthest[0], center.lat, center.lng)
        return radius
    }

    private test(park: Park, parkObj) {
        const result = park.name == parkObj.properties.name
        return result;
    }

    private dist(lat1, lon1, lat2, lon2): number {

        var R = 6371e3; // metres
        var phi1 = this.toRad(lat1);
        var phi2 = this.toRad(lat2);
        var deltaphi = this.toRad(lat2 - lat1)
        var deltalambda = this.toRad(lon2 - lon1)

        var a = Math.sin(deltaphi / 2) * Math.sin(deltaphi / 2) +
            Math.cos(phi1) * Math.cos(phi2) *
            Math.sin(deltalambda / 2) * Math.sin(deltalambda / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        var d = R * c;
        return d;
    }

    private toRad(value: number): number {
        return value * 3.1416 / 180;
    }

}