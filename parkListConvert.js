var turf = require('turf');
var fs = require('fs');

var leisuresObj = JSON.parse(fs.readFileSync('src/assets/leisures.json', 'utf8'));
var parksObj = JSON.parse(fs.readFileSync('src/assets/parks.json', 'utf8'));

var leisures = leisuresObj.elements;
var parks = parksObj.features;

parks = parks.filter(el => el.geometry.type == "Polygon")

leisures = leisures.map(leisure => {

  var pt = turf.point([leisure.lon, leisure.lat]);

  let park = null;
  let found = false;
  for (let i = 0; i < parks.length; i++) {
    park = parks[i];

    var poly = turf.multiPolygon(park.geometry.coordinates)
    found = turf.inside(pt, poly);
    if (found)
      break;
  }

  leisure.parkName = park.properties.name;

  return leisure;

});

fs.writeFile("src/assets/test.json", JSON.stringify(leisures), function (err) {
  if (err) {
    return console.log(err);
  }

  console.log("The file was saved!");
});
